package programa;

import java.util.Scanner;

import clases.Entrada;
import clases.EntradaNormal;
import clases.EntradaVip;
import clases.GestorEntradas;
/**
 * En primer lugar, es necesario importar las clases que se van a utilizar
 * @author mateo
 *
 */
public class Programa {

	/**
	 * Antes de empezar con el menu, creo el administrador que manejara todos los metodos
	 * Tambien inicio un usuario, una entrada y un metodo de asignar, ya que se necesita en el metodo extra 2, el cual es 
	 * listar los usuarios con sus entradas
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		int opcion;
		Entrada entrada = new Entrada();
		GestorEntradas administrador = new GestorEntradas();
		
		administrador.altaEntrada("4585843THE", 76, "Jincho", 400);
		administrador.altaUsuario("Oscar", 18, "5388534F", "2003-03-25");
		administrador.asignarPropietarioEntrada("5388534F", "4585843THE");
		
		do {

			System.out.println("__________________________________________________");
			System.out.println("               Menu              ");
			System.out.println("1.- Dar de alta usuario. ");
			System.out.println("2.- Dar de alta Entrada. ");
			System.out.println("3.- Dar de alta Entrada Normal. ");
			System.out.println("4.- Dar de alta Entrada Vip. ");
			System.out.println("5.- Listar usuario. ");
			System.out.println("6.- Listar cualquier tipo de entrada ");
			System.out.println("7.- Buscar usuario. ");
			System.out.println("8.- Buscar cualquier tipo de entrada ");
			System.out.println("9.- Eliminar un usuario. ");
			System.out.println("10.- Eliminar cualquier tipo de entrada ");
			System.out.println("11.- Metodo extra 1 ");
			System.out.println("12.- Metodo extra 2 ");
			System.out.println("13.- Metodo SuperClase ");
			System.out.println("14.- Metodo SubClase1 ");
			System.out.println("15.- Metodo SubClase2 ");
			System.out.println("16.- Salir");
			System.out.println("___________________________________________________");

			opcion = lector.nextInt();
			lector.nextLine();

			switch (opcion) {

			case 1:
					administrador.altaUsuario("Jesus", 25, "2231223F", "1996-03-02");
					administrador.listarUsuarios();
				break;
			case 2:
					administrador.altaEntrada("34842FTE", 44.5, "Espacio city", 8000);
					administrador.listarEntradas();
				break;

			case 3:
					administrador.altaEntradaNormal("7423774ERGG", 23.23, "Bejo Zaragoza", 300, "Bejo", "XS", 3);
					administrador.listarEntradas();
				break;
			case 4:
					administrador.altaEntradaVip("843828JFN", 67, "Festival Pirata", 22000, "C Tangana", "Yung Beef", "S", 3321);
					administrador.listarEntradas();
				break;
			case 5:
					administrador.listarUsuarios();
				break;
			case 6:
					administrador.listarEntradas();
				break;
			case 7:
				System.out.println("Buscar Usuario");
				System.out.println("Introduce el DNI del usuario");
				String usuario = lector.nextLine();
				System.out.println(administrador.buscarUsuario(usuario));
				break;
			case 8:
				System.out.println("Buscar Entrada");
				System.out.println("Introduce el codigo de la entrada");
				String codigo = lector.nextLine();
				System.out.println(administrador.buscarEntrada(codigo));
				break;
			case 9:
				System.out.println("Eliminar Usuario");
				System.out.println("Introduce el DNI del usuario");
				String usuarioEliminado = lector.nextLine();
				administrador.eliminarUsuario(usuarioEliminado);
				administrador.listarUsuarios();
				break;
			case 10:
				System.out.println("Eliminar Entrada");
				System.out.println("Introduce el codigo de la entrada");
				String entradaEliminada = lector.nextLine();
				administrador.eliminarEntrada(entradaEliminada);
				administrador.listarEntradas();
				break;
			case 11:
				System.out.println("Asignar entrada usuario");
				System.out.println("Introduce el dni del usuario");
				String dni = lector.nextLine();
				System.out.println("Introduce el codigo del concierto");
				String codigo1 = lector.nextLine();
				administrador.asignarPropietarioEntrada(dni, codigo1);
				administrador.listarEntradasUsuarios(dni);
				break;
			case 12:
				System.out.println("Listar entradas de usuarios");
				administrador.listarEntradasUsuarios("5388534F");
				break;
			case 13:
				administrador.rellenarEntradaTeclado(entrada);
				administrador.listarEntradas();
				break;
			case 14:
				/**
				 * Para poder usar este metodo, es necesario introducir el codigo de una entrada VIP, ya que esta posee precio extra
				 * En las altas de arriba, se encuentra una
				 */
				System.out.println("Introduzca el codigo de la entrada VIP");
				String codigo2 = lector.nextLine();
				System.out.println("Introduce el precio extra");
				double precioExtra = lector.nextDouble();
				lector.nextLine();
				administrador.precioVip((EntradaVip)administrador.buscarEntrada(codigo2), precioExtra);
				System.out.println(administrador.buscarEntrada(codigo2));
				break;
			case 15:
				/**
				 * Igual que en el anterior caso, se necesita una entrada normal para poder usar este metodo
				 *  En las altas de arriba, se encuentra una
				 */
				System.out.println("Introduzca el codigo de la entrada Normal");
				String codigo3 = lector.nextLine();
				System.out.println("Introduce la talla nueva de tu camiseta");
				String tallaNueva = lector.nextLine();
				administrador.cambioCamiseta((EntradaNormal)administrador.buscarEntrada(codigo3), codigo3, tallaNueva);
				System.out.println(administrador.buscarEntrada(codigo3));
				break;
			case 16:
				System.exit(0);
				break;
			default:
				System.out.println("Opcion no contemplada");
				break;
			}
		} while (opcion != 16);

		lector.close();

	}

}
