package clases;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * La clase entrada en este caso sera una super clase
 * @author mateo
 *
 */
public class Entrada {
	
	Scanner lector = new Scanner(System.in);
	
	String codigo;
	double precio;
	String nombreConcierto;
	LocalDate fecha;
	int aforo;
	Usuario propietario;
	
	public Entrada() {
		super();
		this.codigo = "";
		this.precio = 0;
		this.nombreConcierto = "";
		this.aforo = 0;
	}
	
	public Entrada(String codigo, double precio, String nombreConcierto, int aforo) {
		super();
		this.codigo = codigo;
		this.precio = precio;
		this.nombreConcierto = nombreConcierto;
		this.aforo = aforo;
	}
	
	public Entrada(String codigo, double precio, String nombreConcierto, LocalDate fecha, int aforo,
			Usuario propietario) {
		super();
		this.codigo = codigo;
		this.precio = precio;
		this.nombreConcierto = nombreConcierto;
		this.fecha = fecha;
		this.aforo = aforo;
		this.propietario = propietario;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getNombreConcierto() {
		return nombreConcierto;
	}

	public void setNombreConcierto(String nombreConcierto) {
		this.nombreConcierto = nombreConcierto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public int getAforo() {
		return aforo;
	}

	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	public Usuario getPropietario() {
		return propietario;
	}

	public void setPropietario(Usuario propietario) {
		this.propietario = propietario;
	}
	
	@Override
	public String toString() {
		return "Entrada [codigo=" + codigo + ", precio=" + precio + ", nombreConcierto=" + nombreConcierto + ", fecha="
				+ fecha + ", aforo=" + aforo + ", propietario=" + propietario + "]";
	}
	
	/**
	 * Creo el metodo rellenar, para poder rellenar una entrada por teclado
	 * Simplemente es una prueba, ya que no se puede dar de alta directamente
	 * Por ello la inicializo antes del main en el programa
	 * @return
	 */
	public boolean rellenar() {
		boolean error = false;
		do {
			try {
				System.out.println("Introduce el codigo del concierto");
				this.codigo = lector.nextLine();
				System.out.println("Introduce el precio del concierto");
				this.precio = lector.nextDouble();
				lector.nextLine();
				System.out.println("Introduce el nombre del concierto");
				this.nombreConcierto = lector.nextLine();
				System.out.println("Introduce el aforo del concierto");
				this.aforo = lector.nextInt();
				error = false;
			} catch (InputMismatchException e) {
				System.out.println("No se han introducido correctamente los datos");
				error = true;
				lector.nextLine();
			} catch (Exception e) {
				System.out.println("Fallo general");
				error = true;
				lector.nextLine();
			}

		} while (error);
		return false;

	}
}
