package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
/**
 * El gestor se encarga de realizar todos los metodos, inicializando dos arraylist de las clases usuario y entrada
 * Solamente dos, ya que las subclases entraran dentro del array de entrada
 * Por ello, cuando listemos, listaremos todas las entradas, no solo las normales o las vip
 * @author mateo
 *
 */
public class GestorEntradas {
	private ArrayList<Usuario> listaUsuarios;
	private ArrayList<Entrada> listaEntradas;

	public GestorEntradas() {
		listaUsuarios = new ArrayList<Usuario>();
		listaEntradas = new ArrayList<Entrada>();
	}

	public void altaUsuario(String nombre, int edad, String dni, String fechaNacimiento) {
		Usuario nuevoUsuario = new Usuario(nombre, edad, dni);
		nuevoUsuario.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
		listaUsuarios.add(nuevoUsuario);
	}

	public void listarUsuarios() {
		for (Usuario usuario : listaUsuarios) {
			if (usuario != null) {
				System.out.println(usuario);
			}
		}

	}

	public Usuario buscarUsuario(String dni) {
		for (Usuario usuario : listaUsuarios) {
			if (usuario != null && usuario.getDni().equals(dni)) {
				return usuario;
			}
		}
		return null;
	}

	public void eliminarUsuario(String dni) {
		Iterator<Usuario> iteradorUsuarios = listaUsuarios.iterator();

		while (iteradorUsuarios.hasNext()) {
			Usuario usuario = iteradorUsuarios.next();
			if (usuario.getDni().equals(dni)) {
				iteradorUsuarios.remove();
			}
		}

	}

	public void altaEntrada(String codigo, double precio, String nombreConcierto, int aforo) {
		Entrada nuevaEntrada = new Entrada(codigo, precio, nombreConcierto, aforo);
		listaEntradas.add(nuevaEntrada);
	}

	public void altaEntradaNormal(String codigo, double precio, String nombreConcierto, int aforo, String nombreArtista,
			String tallaCamisetaRegalo, int costesGestion) {
		EntradaNormal nuevaEntradaNormal = new EntradaNormal(codigo, precio, nombreConcierto, aforo, nombreArtista,
				tallaCamisetaRegalo, costesGestion);
		listaEntradas.add(nuevaEntradaNormal);
	}

	public void altaEntradaVip(String codigo, double precio, String nombreConcierto, int aforo, String nombreArtista,
			String nombreArtistaExtra,  String tallaCamisetaRegalo, int codZonaVip) {
		EntradaVip nuevaEntradaVip = new EntradaVip(codigo, precio, nombreConcierto, aforo, nombreArtista,
				nombreArtistaExtra, tallaCamisetaRegalo, codZonaVip);
		listaEntradas.add(nuevaEntradaVip);
	}

	public void listarEntradas() {
		for (Entrada entrada : listaEntradas) {
			if (entrada != null) {
				System.out.println(entrada);
			}
		}
	}

	public Entrada buscarEntrada(String codigo) {
		for (Entrada entrada : listaEntradas) {
			if (entrada != null && entrada.getCodigo().equals(codigo)) {
				return entrada;
			}
		}
		return null;
	}

	public void eliminarEntrada(String codigo) {
		Iterator<Entrada> iteradorEntradas = listaEntradas.iterator();
		while (iteradorEntradas.hasNext()) {
			Entrada entrada = iteradorEntradas.next();
			if (entrada.getCodigo().equals(codigo)) {
				iteradorEntradas.remove();
			}
		}

	}

	public void asignarPropietarioEntrada(String dni, String codigo) {
		Usuario usuario = buscarUsuario(dni);
		Entrada entrada = buscarEntrada(codigo);
		entrada.setPropietario(usuario);
	}

	public void listarEntradasUsuarios(String dni) {
		for (Entrada entrada : listaEntradas) {
			if (entrada.getPropietario() != null && entrada.getPropietario().getDni().equals(dni)) {
				System.out.println(entrada);
			}
		}
	}

	public void rellenarEntradaTeclado(Entrada entrada) {
		entrada.rellenar();
		listaEntradas.add(entrada);
	}

	public void precioVip(EntradaVip entradaV, double precioExtra) {
		entradaV.precioVip(precioExtra);
	}

	public void cambioCamiseta(EntradaNormal entradaN, String codigo, String tallaNueva) {
		entradaN.cambioCamiseta(codigo, tallaNueva);
	}
}
