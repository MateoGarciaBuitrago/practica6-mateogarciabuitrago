package clases;

/**
 * La clase entrada Normal sera una subclase, que hereda atributos de entrada
 * @author mateo
 *
 */

public class EntradaNormal extends Entrada {

	String nombreArtista;
	String tallaCamisetaRegalo;
	int costesGestion;
	
	public EntradaNormal() {
		super();
		this.nombreArtista = "";
		this.tallaCamisetaRegalo = "";
		this.costesGestion = 0;
	}
	
	
	public EntradaNormal(String codigo, double precio, String nombreConcierto, int aforo, String nombreArtista, String tallaCamisetaRegalo, int costesGestion) {
		super(codigo,precio,nombreConcierto,aforo);
		this.nombreArtista = nombreArtista;
		this.tallaCamisetaRegalo = tallaCamisetaRegalo;
		this.costesGestion = costesGestion;
	}

	public String getNombreArtista() {
		return nombreArtista;
	}


	public void setNombreArtista(String nombreArtista) {
		this.nombreArtista = nombreArtista;
	}


	public String getTallaCamisetaRegalo() {
		return tallaCamisetaRegalo;
	}


	public void setTallaCamisetaRegalo(String tallaCamisetaRegalo) {
		this.tallaCamisetaRegalo = tallaCamisetaRegalo;
	}


	public int getIva() {
		return costesGestion;
	}


	public void setIva(int costesGestion) {
		this.costesGestion = costesGestion;
	}
	
	@Override
	public String toString() {
		return "EntradaNormal [nombreArtista=" + nombreArtista + ", tallaCamisetaRegalo=" + tallaCamisetaRegalo
				+ ", iva=" + costesGestion + ", codigo=" + codigo + ", precio=" + precio + ", nombreConcierto=" + nombreConcierto
				+ ", fecha=" + fecha + ", aforo=" + aforo + ", propietario=" + propietario + "]";
	}
	
	/**
	 * Este simple metodo, recibe el codigo de la entrada y cambia la talla de la camiseta que recibira el usuario
	 * @param codigo
	 * @param tallaNueva
	 */
	public void cambioCamiseta (String codigo, String tallaNueva) {
		if (this.codigo.equals(codigo)) {
			this.setTallaCamisetaRegalo(tallaNueva);
		}
	}
}
