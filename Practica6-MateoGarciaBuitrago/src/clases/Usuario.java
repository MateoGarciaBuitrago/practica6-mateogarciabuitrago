package clases;

import java.time.LocalDate;

/**
 * Creo la clase1, la cual adquiere entradas
 * @author mateo
 *
 */

public class Usuario {
	private String nombre;
	private int edad;
	private String dni;
	private LocalDate fechaNacimiento;
	
	public Usuario() {
		super();
		this.nombre = "";
		this.edad = 0;
		this.dni = "";
		
	}
	
	public Usuario(String nombre, int edad, String dni) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", fechaNacimiento=" + fechaNacimiento
				+ "]";
	}
}
