package clases;

/**
 * La entrada Vip sera la subclase 2 y tambien hereda de entrada
 * @author mateo
 *
 */
public class EntradaVip extends Entrada{
	
	
	String nombreArtista;
	String nombreArtistaExtra;
	int precioExtra;
	String tallaCamisetaRegalo;
	int codZonaVip;
	
	
	public EntradaVip() {
		super();
		this.nombreArtista = "";
		this.nombreArtistaExtra = "";
		this.precioExtra = 0;
		this.tallaCamisetaRegalo = "";
		this.codZonaVip = 0;
	}
	
	public EntradaVip(String codigo, double precio, String nombreConcierto, int aforo, String nombreArtista, String nombreArtistaExtra, String tallaCamisetaRegalo,
			int codZonaVip) {
		super(codigo,precio,nombreConcierto,aforo);
		this.nombreArtista = nombreArtista;
		this.nombreArtistaExtra = nombreArtistaExtra;
		this.tallaCamisetaRegalo = tallaCamisetaRegalo;
		this.codZonaVip = codZonaVip;
	}

	public String getNombreArtista() {
		return nombreArtista;
	}

	public void setNombreArtista(String nombreArtista) {
		this.nombreArtista = nombreArtista;
	}

	public String getNombreArtistaExtra() {
		return nombreArtistaExtra;
	}

	public void setNombreArtistaExtra(String nombreArtistaExtra) {
		this.nombreArtistaExtra = nombreArtistaExtra;
	}

	public int getPrecioExtra() {
		return precioExtra;
	}

	public void setPrecioExtra(int precioExtra) {
		this.precioExtra = precioExtra;
	}

	public String getTallaCamisetaRegalo() {
		return tallaCamisetaRegalo;
	}

	public void setTallaCamisetaRegalo(String tallaCamisetaRegalo) {
		this.tallaCamisetaRegalo = tallaCamisetaRegalo;
	}

	public int getCodZonaVip() {
		return codZonaVip;
	}

	public void setCodZonaVip(int codZonaVip) {
		this.codZonaVip = codZonaVip;
	}

	@Override
	public String toString() {
		return "EntradaVip [nombreArtista=" + nombreArtista + ", nombreArtistaExtra=" + nombreArtistaExtra
				+ ", precioExtra=" + precioExtra + ", tallaCamisetaRegalo=" + tallaCamisetaRegalo + ", codZonaVip="
				+ codZonaVip + ", codigo=" + codigo + ", precio=" + precio + ", nombreConcierto=" + nombreConcierto
				+ ", fecha=" + fecha + ", aforo=" + aforo + ", propietario=" + propietario + "]";
	}
	
	/**
	 * Este metodo unicamente incrementa el precio final de la entrada, ya que es VIP
	 * @param precioExtra
	 * @return
	 */
	public double precioVip (double precioExtra) {
		precio+=precioExtra;
		return precio;
	}
}
